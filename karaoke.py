#!/usr/bin/python3
# -*- coding: utf-8 -*-


from xml.sax import make_parser
import sys
import json
from smallsmilhandler import SmallSMILHandler
import urllib.request


class KaraokeLocal:

    def __init__(self, archivo):

        try:
            parser = make_parser()
            cHandler = SmallSMILHandler()
            parser.setContentHandler(cHandler)
            parser.parse(open(archivo))  # Abrimos el archivo de la shell
            self.lista = cHandler.get_tags()  # Creamos lista de etiquetas

        except FileNotFoundError:  # Error no se especifica archivo(shell)
            sys.exit("No se ha encontrado el archivo.")

    def __str__(self):

        lineas = ""
        for etiqueta in self.lista:
            lineas += etiqueta[0]
            for atr in etiqueta[1]:
                if etiqueta[1][atr] != "":
                    lineas += '\t' + atr + '="' + etiqueta[1][atr] + '"'
            lineas += '\n'  # Suma los elementos de la linea y salta a otra
        return lineas  # Nos devuelve las lineas

    def to_json(self, archivosmil, archivojson=""):  # Pasar de smill a json

        if archivojson == "":
            archivojson = archivosmil.replace('.smil', '.json')

        with open(archivojson, 'w') as jsonfile:  # Abrimos el json
            json.dump(self.lista, jsonfile, indent=3)  # Copia las etiquetas

    def do_local(self):

        for etiqueta in self.lista:
            for atributo in etiqueta[1]:
                if atributo == 'src':  # Busca por la etiqueta src
                    if etiqueta[1][atributo].startswith('http'):
                        mul = etiqueta[1][atributo].split('/')[-1]
                        urllib.request.urlretrieve(etiqueta[1][atributo], mul)
                        etiqueta[1][atributo] = mul
                        # La funcion do_local, busca la atributo src,
                        # lo identifica por http,
                        # lo divide mediante las /, coge la ultima parte
                        # donde esta  el archivo y
                        # lo descarga de manera local.


if __name__ == "__main__":
    """
    Programa principal
    """
    try:
        archivo = sys.argv[1]  # Define archivo como el 1ª argumento de lashell
    except IndexError:
        sys.exit("Usage: python3 karaoke.py file.smil")
    archivofinal = KaraokeLocal(archivo)
    print(archivofinal)  # Imprimimos por al shell el archivo en smil
    archivofinal.to_json(archivo)  # Pasamos el archivo a json
    archivofinal.do_local()
    archivofinal.to_json(archivo, 'local.json')  # Guarda local.json en local
    print(archivofinal)  # Imprimimos por la shell el archivo en json
