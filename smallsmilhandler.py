#!/usr/bin/python3
# -*- coding: utf-8 -*-
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):

    def __init__(self):

        self.lista = []  # Creamos la lista

        self.valores = {'root-layout': ['width', 'height', 'background-color'],
                        'region': ['id', 'top', 'bottom', 'left', 'right'],
                        'img': ['src', 'region', 'begin', 'dur'],
                        'audio': ['src', 'begin', 'dur'],
                        'textstream': ['src', 'region']}

    def startElement(self, name, atributos):

        diccionario = {}  # Creamos el diccionario

        if name in self.valores:
            for att in self.valores[name]:
                diccionario[att] = atributos.get(att, '')  # Almacena los atri-
            self.lista.append([name, diccionario])  # butos en el diccionario

    def get_tags(self):

        return self.lista  # Nos devuelve la lista


if __name__ == "__main__":
    """
    Programa principal
    """
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    print(cHandler.get_tags())
